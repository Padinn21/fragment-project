package com.example.learnandro3.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.learnandro3.R
import com.example.learnandro3.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSecondBinding.inflate(layoutInflater, container, false)
        return (binding.root)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //mendapatkan data
        val aName = arguments?.getString("EXTRA_NAME")
        binding.txtTvName.text = "Your Name: $aName"

        binding.btnPg3.setOnClickListener{
            if (binding.txtEtName.text.isNullOrEmpty()) {
                Toast.makeText((requireContext()),"Empty Name", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val actionToThirdFragment =
                    SecondFragmentDirections.actionSecondFragment2ToThirdFragment3()
                actionToThirdFragment.name = binding.txtEtName.text.toString()
                view.findNavController().navigate(actionToThirdFragment)
            }
        }
    }

}
